TF_ORGANIZATION= $1
TF_TOKEN= $2
WORKSPACE_NAME= $3

WORKSPACE_EXISTS=$(curl \
  --header "Authorization: Bearer $TF_TOKEN" \
  --header "Content-Type: application/vnd.api+json" \
  --request GET \
  "https://app.terraform.io/api/v2/organizations/$TF_ORGANIZATION/workspaces/$WORKSPACE_NAME" \
  --silent \
  --output /dev/null \
  --write-out "%{http_code}")

if [ "$WORKSPACE_EXISTS" = "404" ]; then
  echo "$WORKSPACE_NAME Workspace does not exist, creating..."
  curl \
    --header "Authorization: Bearer $TF_TOKEN" \
    --header "Content-Type: application/vnd.api+json" \
    --request POST \
    --data "{\"data\":{\"type\":\"workspaces\",\"attributes\":{\"name\":\"$WORKSPACE_NAME\",\"auto-apply\":false}}}" \
    "https://app.terraform.io/api/v2/organizations/$TF_ORGANIZATION/workspaces"
else
  echo "$WORKSPACE_NAME Workspace already exists, skipping creation."
fi
